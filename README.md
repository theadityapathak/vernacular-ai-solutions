# Vernacular-ai-solutions

## Steps to recreate:

#### 1. Docker

Dependencies:

- [X] **Docker**

steps to run docker image:
i) build the image
```shell
docker build -t <image-name> <path_to_Dockerfile>
```
ii) run the image in integrated mode
```shell
docker run -it <image-name>
```

#### 2. Auditing Hardware

Bash-script Hardware-Details.sh is added to provide all the details using pre installed commands in RHEL 8.2

Run bash script:

```shell
bash Hardware-Details.sh
```

#### 3. Managing Disk Space

For managing disk space , added a bash script called nfs-disk.sh , it get the details of crestion time using debufs command line tool and then check the condition thatcreation time of file should be 2 days ago as well as it is in .wav format , then it creates a log file with the name deleted-files-<date>-<month>-<year>.log containig details of deleted media files.

Run bash script:

```shell
bash nfs-disk.sh
```



