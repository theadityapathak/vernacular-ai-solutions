#!/bin/bash
# to gte os version
os_version=`cat /etc/os-release | grep ^VERSION=`
echo "OS ${os_version}"
# to get kernel name
os_kernel=`uname -r`
echo "KERNEL = ${os_kernel}"
#to get the number of virtual core
echo "Virtual core = $(nproc --all)"
#cpu speed details
cpu_speed=`cat /proc/cpuinfo | grep "cpu MHz" | awk '{print $4}'`
echo "CPU SPEED = ${cpu_speed} MHz"
#Architecture details
echo "Architecture = $(uname -i)"
#to get total RAM
echo "Total RAM = $(cat /proc/meminfo | grep MemTotal | awk '{print $2}') kb"
#network bandwith details i.e Recieve and transmit of packets
echo -e "Network Bandwith = \n $(netstat --interfaces=eth0  |awk '{ print $3 "\t" $7 }')"
#os based firewall details , here we assuming our firewall as iptables
echo -e "OS Firewall (iptables) = \n $(iptables -L) 
#to get disk details i.e disk name , major , minor, size , type,mountpoint
echo -e "Disk Details = \n $(lsblk) "