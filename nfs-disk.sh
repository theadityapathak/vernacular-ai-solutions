#/bin/bash
#path of directory which contains audio files
Path_of_Directory=/home/ubuntu/audios/
#Date for 2 days ago
Date2=`date --date="2 hour ago" "+%h%d "`
#getting inode number of files.
inode=`stat $Path_of_Directory | grep Inode | awk -F" " '{ print $4 }'`
#getting filesystem in which file is present.
fileSystem=`df $Path_of_Directory | awk '{print $1}' | sed 1d`
#getting creation time of file.
detail=`debugfs -R "stat <${inode}>" $fileSystem | grep ctime | awk '{ print $5 $6 }'`
format=`ls *.wav`
#comparing file format as well as Date of creation with a 2 days ago variable , if both applies.
if [ "${Date2}" == "$detail" ] && [ "${format}" ==! "" ];
then      
  echo file is 2 days ago , thus deleting it.
  file_name=deleted-files-$(date +"%d-%m-%Y")
  ${Path_of_Directory} ${detail} $now > $file_name
  rm -rf ${Path_of_Directory}.wav
  if [ "${Date2}" == "" ];
  then
    echo "You do not have files older than 2 Days"
  fi
fi